## Installation
1. Configure variables in _.env_
    1. `APP_ENV=prod`
2. Install/update your vendors: `composer install --no-dev --optimize-autoloader`
3. Clear cache: `php bin/console cache:clear`

## Console commands
1. Github repository view: `php bin/console github:repository {ownerLogin} {repositoryName}`
2. Github user view: `php bin/console github:user {login}`
