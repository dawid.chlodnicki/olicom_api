<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GithubRepositoryCommand extends BaseGithubCommand
{
    /** @var string */
    protected static $defaultName = 'github:repository';

    protected function configure(): void
    {
        $this->addArgument('ownerLogin', InputArgument::REQUIRED);
        $this->addArgument('repositoryName', InputArgument::REQUIRED);
    }

    /**
     * @param  InputInterface  $input
     * @param  OutputInterface  $output
     * @return int
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $ownerLogin = $input->getArgument('ownerLogin');
        $repositoryName = $input->getArgument('repositoryName');
        $viewModel = $this->service->repository($ownerLogin, $repositoryName);

        if ($viewModel !== null) {
            $this->writeViewModel($viewModel, $input, $output);
            return Command::SUCCESS;
        }

        $this->writeError('Not found', $output);
        return Command::FAILURE;
    }
}