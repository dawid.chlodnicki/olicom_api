<?php


namespace App\Command;


use App\Services\GithubService;
use App\ViewModels\BaseViewModel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class BaseGithubCommand extends Command
{
    /** @var GithubService */
    protected $service;

    public function __construct(GithubService $service, string $name = null)
    {
        parent::__construct($name);

        $this->service = $service;
    }

    /**
     * @param BaseViewModel $viewModel
     * @param $input
     * @param $output
     */
    protected function writeViewModel(BaseViewModel $viewModel, InputInterface $input, OutputInterface $output): void
    {
        $rows = [];

        foreach ((array)$viewModel as $key => $value) {
            $rows[] = [$key, $value];
        }

        $io = new SymfonyStyle($input, $output);
        $io->table(['Key', 'Value'], $rows);
    }

    /**
     * @param  string  $message
     * @param  OutputInterface  $output
     */
    protected function writeError(string $message, OutputInterface $output): void
    {
        $output->writeln("<error>{$message}</error>");
    }
}