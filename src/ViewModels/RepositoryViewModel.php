<?php


namespace App\ViewModels;


class RepositoryViewModel extends BaseViewModel
{
    /** @var string */
    public $fullName;

    /** @var string */
    public $description;

    /** @var int */
    public $stars;

    /** @var string */
    public $cloneUrl;

    /** @var string */
    public $createdAt;
}