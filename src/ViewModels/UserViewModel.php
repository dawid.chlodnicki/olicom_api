<?php


namespace App\ViewModels;


class UserViewModel extends BaseViewModel
{
    /** @var string */
    public $name;

    /** @var string */
    public $url;

    /** @var string */
    public $email;

    /** @var string */
    public $createdAt;
}