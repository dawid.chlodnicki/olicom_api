<?php


namespace App\Services;


use App\ViewModels\RepositoryViewModel;
use App\ViewModels\UserViewModel;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GithubService extends BaseService
{
    /** @var HttpClientInterface */
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param  string  $ownerLogin
     * @param  string  $repositoryName
     * @return RepositoryViewModel|null
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function repository(string $ownerLogin, string $repositoryName): ?RepositoryViewModel
    {
        $response = $this->client->request('GET', "https://api.github.com/repos/{$ownerLogin}/{$repositoryName}");

        if ($response->getStatusCode() !== 200) {
            return null;
        }

        $data = $response->toArray();
        $viewModel = new RepositoryViewModel();
        $viewModel->fullName = $data['full_name'];
        $viewModel->description = $data['description'];
        $viewModel->stars = $data['stargazers_count'];
        $viewModel->cloneUrl = $data['clone_url'];
        $viewModel->createdAt = $data['created_at'];

        return $viewModel;
    }

    /**
     * @param  string  $login
     * @return UserViewModel|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function user(string $login): ?UserViewModel
    {
        $response = $this->client->request('GET', "https://api.github.com/users/{$login}");

        if ($response->getStatusCode() !== 200) {
            return null;
        }

        $data = $response->toArray();
        $viewModel = new UserViewModel();
        $viewModel->name = $data['name'];
        $viewModel->url = $data['html_url'];
        $viewModel->email = $data['email'];
        $viewModel->createdAt = $data['created_at'];

        return $viewModel;
    }
}