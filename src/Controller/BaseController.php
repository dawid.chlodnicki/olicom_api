<?php


namespace App\Controller;


use App\Services\BaseService;

abstract class BaseController
{
    /** @var BaseService */
    protected $service;
}