<?php


namespace App\Controller;


use App\Services\GithubService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class GithubController extends BaseController
{
    /**
     * GithubController constructor.
     * @param  GithubService  $service
     */
    public function __construct(GithubService $service)
    {
        $this->service = $service;
    }

    /**
     * @param  string  $ownerLogin
     * @param  string  $repositoryName
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function repository(string $ownerLogin, string $repositoryName): JsonResponse
    {
        $response = new JsonResponse();
        $viewModel = $this->service->repository($ownerLogin, $repositoryName);

        if ($viewModel !== null) {
            return $response->setData($viewModel);
        }

        return $response->setStatusCode(404);
    }

    /**
     * @param  string  $login
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function user(string $login): JsonResponse
    {
        $response = new JsonResponse();
        $viewModel = $this->service->user($login);

        if ($viewModel !== null) {
            return $response->setData($viewModel);
        }

        return $response->setStatusCode(404);
    }
}