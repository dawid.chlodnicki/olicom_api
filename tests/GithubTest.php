<?php


namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GithubTest extends WebTestCase
{
    public function testViewRepository(): void
    {
        $client = static::createClient();
        $response = $client->request('GET', '/repositories/laravel/laravel');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testViewUser(): void
    {
        $client = static::createClient();
        $response = $client->request('GET', '/users/laravel');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}